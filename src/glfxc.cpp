/* Copyright (c) 2011, Max Aizenshtein <max.sniffer@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

#include <stdio.h>
#include <iostream>
//#include <GL/glew.h>
//#include <GL/freeglut.h>
#include <GL/glfx.h>

#include <QOpenGLContext>
#include <QGuiApplication>
#include <QOpenGLWindow>

class ShaderWindow : public QOpenGLWindow {
private:
    char *path;
    char *name;
public:
    ShaderWindow(char *path, char *name);
    void initializeGL();
    void resizeGL();
    void paintGL();
};

ShaderWindow::ShaderWindow(char *path, char *name) {
    this->path = path;
    this->name = name;
    QSurfaceFormat format;
    format.setSamples(16);
    format.setMajorVersion(4);
    format.setMinorVersion(1);
    setFormat(format);
}

void ShaderWindow::initializeGL() {
         QOpenGLContext *context = QOpenGLContext::currentContext();
    std::cout << "*** OpenGL information:" << std::endl;
    std::cout << "OpenGL context is "
                    << (context->isValid() ? "" : "NOT ") << "valid." << std::endl;
    std::cout << "\tRequested version: 4.1" << std::endl;
    std::cout << "\tObtained version: "
                    << context->format().majorVersion() << "."
                    << context->format().minorVersion() << std::endl;
    std::cout << "\tObtained profile: "
                    << context->format().profile() << std::endl;
    
    int effect = glfxGenEffect();
    
    char log[10000];
    
    if (!glfxParseEffectFromFile(effect, path)) {
        printf("Error creating effect:\n");
        glfxGetEffectLog(effect, log, sizeof(log));
        printf("%s\n", log);
        glfxDeleteEffect(effect);
        close();
        return;
    }
    
    if (glfxCompileProgram(effect, name) < 0) {
        printf("Error compiling program '%s':\n", name);
        glfxGetEffectLog(effect, log, sizeof(log));
        printf("%s\n", log);
        glfxDeleteEffect(effect);
        close();
        return;
    }
    
    printf("Compiled successfully\n");
}

void ShaderWindow::resizeGL() {}

void ShaderWindow::paintGL() {}
}

int main(int argc, char** argv) 
{
    if (argc != 3) {
        printf("Usage: %s <effect file> <program name>\n", argv[0]);
        return 0;
    }

    QGuiApplication app(argc, argv);

    ShaderWindow window(argv[1], argv[2]);
    window.show();

    return app.exec();
}